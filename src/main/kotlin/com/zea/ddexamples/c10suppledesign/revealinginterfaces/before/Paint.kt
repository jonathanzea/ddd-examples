package com.zea.ddexamples.c10suppledesign.revealinginterfaces.before

class Paint(var v: Double, var r: Int, var y: Int, var b: Int) {

    fun paint(paint: Paint) {
        v += paint.v
        // complicated color mixin algorithm
        // reduced to mocked behaviour
        b = 25
        y = 25
        r - 0
    }
}
