package com.zea.ddexamples.c10suppledesign.assertions.before

class Paint(var volume: Double, var pigmentColor: PigmentColor) {

    fun mixIn(paint: Paint) {
        volume += paint.volume
        pigmentColor = pigmentColor.mixedWith(paint.pigmentColor)
    }
}
