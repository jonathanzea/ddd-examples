package com.zea.ddexamples.c10suppledesign.assertions.before

data class PigmentColor(var red: Int, var yellow: Int, var blue: Int) {

    fun mixedWith(pigmentColor: PigmentColor): PigmentColor {
        // complicated color mixin algorithm
        // reduced to mocked behaviour
        return PigmentColor(0, 25, 25)
    }
}
