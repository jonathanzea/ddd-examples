package com.zea.dddexamples.c10suppledesign.assertions.after

import com.zea.ddexamples.c10suppledesign.assertions.after.PigmentColor

class StockPaint(volume: Double, color: PigmentColor) : Paint {
    override val volume: Double = volume
    override val pigmentColor: PigmentColor = color
}
