package com.zea.dddexamples.c10suppledesign.assertions.after

import com.zea.ddexamples.c10suppledesign.assertions.after.PigmentColor

interface Paint {
    val volume: Double
    val pigmentColor: PigmentColor
}
