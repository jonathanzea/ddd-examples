package com.zea.dddexamples.c10suppledesign.assertions.after

import com.zea.ddexamples.c10suppledesign.assertions.after.PigmentColor

class MixedPaint(constituents: List<StockPaint>) : Paint {

    override val volume: Double = constituents.map { it.volume }.sum()
    override val pigmentColor: PigmentColor =
        // complicated color mixin algorithm iterating over the `constituents` reduced to mocked behaviour
        PigmentColor(0, 25, 25)
}
