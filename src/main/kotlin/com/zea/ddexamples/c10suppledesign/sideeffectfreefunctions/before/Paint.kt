package com.zea.ddexamples.c10suppledesign.sideeffectfreefunctions.before

class Paint(var volume: Double, var red: Int, var yellow: Int, var blue: Int) {

    fun mixIn(paint: Paint) {
        volume += paint.volume
        // complicated color mixin algorithm
        // reduced to mocked behaviour
        blue = 25
        yellow = 25
        red - 0
    }
}
