package com.zea.ddexamples.c10suppledesign.sideeffectfreefunctions.after

data class PigmentColor(var red: Int, var yellow: Int, var blue: Int) {

    fun mixedWith(pigmentColor: PigmentColor): PigmentColor {
        // complicated color mixin algorithm
        // reduced to mocked behaviour
        pigmentColor.blue // ...
        return PigmentColor(0, 25, 25)
    }
}
