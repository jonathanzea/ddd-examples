package com.zea.ddexamples.c10suppledesign.sideeffectfreefunctions.after

class Paint(var volume: Double, var pigmentColor: PigmentColor) {

    fun mixIn(paint: Paint) {
        volume += paint.volume
        pigmentColor = pigmentColor.mixedWith(paint.pigmentColor)
    }
}
