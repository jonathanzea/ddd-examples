package com.zea.ddexamples.c9makingimplicitconceptsexplicit.specification

class Drum(private val chemical: Chemical) {

    fun chemicalSpecification(): ContainerSpecification {
        return chemical.containerSpecification
    }
}
