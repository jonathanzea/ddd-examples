package com.zea.ddexamples.c9makingimplicitconceptsexplicit.specification

class Container {

    private val features: MutableList<ContainerFeature> = mutableListOf()

    fun withFeature(feature: ContainerFeature): Container {
        features.add(feature)
        return this
    }

    fun containsFeature(requiredFeature: ContainerFeature): Boolean {
        return features.contains(requiredFeature)
    }
}
