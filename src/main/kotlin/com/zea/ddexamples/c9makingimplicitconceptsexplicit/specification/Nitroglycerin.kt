package com.zea.ddexamples.c9makingimplicitconceptsexplicit.specification

class Nitroglycerin : Chemical() {

    override val containerSpecification: ContainerSpecification
        get() = ContainerSpecification(ContainerFeature.ARMORED)
}
