package com.zea.ddexamples.c9makingimplicitconceptsexplicit.specification

abstract class Chemical {
    abstract val containerSpecification: ContainerSpecification
}
