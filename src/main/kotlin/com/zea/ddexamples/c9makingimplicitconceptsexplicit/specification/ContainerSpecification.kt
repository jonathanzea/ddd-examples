package com.zea.ddexamples.c9makingimplicitconceptsexplicit.specification

class ContainerSpecification(private val requiredFeature: ContainerFeature) {

    fun isSatisfiedBy(container: Container): Boolean {
        return container.containsFeature(requiredFeature)
    }
}
