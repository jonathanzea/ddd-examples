package com.zea.ddexamples.c5amodelexpressedinsoftware.before

class BrokerageAccount(private val id: String, private val customer: Customer) {
    private val investments: MutableList<Investment> =
        mutableListOf()

    fun order(stockSymbol: String, numberOfShares: Int) {
        investments.add(
            Investment(
                stockSymbol,
                numberOfShares
            )
        )
    }

    fun getInvestmentsSummary() =
        investments.joinToString { "Customer: ${customer.ssNumber} / Account: $id - ${it.stockSymbol} x${it.numberOfShares}" }

    /*
    val query: String = "SELECT * FROM investment WHERE " +
        "brokerage_account_id = '$id' "
       ...
     */
}
