package com.zea.ddexamples.c5amodelexpressedinsoftware.after

data class Investment(val stockSymbol: String, val numberOfShares: Int)
