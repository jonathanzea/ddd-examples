package com.zea.ddexamples.c5amodelexpressedinsoftware.after

class BrokerageAccount(private val id: String, private val customer: Customer) {

    private val investments = mutableMapOf<String, Investment>()

    fun order(investment: Investment) {
        investments[investment.stockSymbol] = investment
    }

    fun getInvestment(stockSymbol: String): String =
        "Customer ${customer.ssNumber} / Account: $id - $stockSymbol: ${investments[stockSymbol]!!.numberOfShares}"

    /*
     val query: String = "SELECT * FROM investment WHERE " +
         "brokerage_account_id = '$id' " +
         "AND stock_symbol = '$stockSymbol'
        ...
      */
}
