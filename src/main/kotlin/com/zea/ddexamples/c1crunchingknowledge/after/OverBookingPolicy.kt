package com.zea.ddexamples.c1crunchingknowledge.after

class OverBookingPolicy {
    fun isAllowed(voyage: Voyage, cargo: Cargo): Boolean {
        return voyage.defaultCargo.size + cargo.size <= (voyage.capacity * 1.1)
    }
}
