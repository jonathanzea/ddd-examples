package com.zea.ddexamples.c1crunchingknowledge.after

class ShipmentService(private val overbookingPolicy: OverBookingPolicy) {

    fun makeBooking(voyage: Voyage, cargo: Cargo): String {
        return if (overbookingPolicy.isAllowed(voyage, cargo)) {
            voyage.addCargo(cargo)

            "Booking SUCCESSFULLY made - Voyage (${voyage.itinerary}) - " +
                    "(default cargo: ${voyage.defaultCargo.content} - " +
                    "${voyage.defaultCargo.size}Kg) + (${cargo.content} - ${cargo.size}Kg)"
        } else {
            "NOT POSSIBLE - Voyage (${voyage.itinerary}) - " +
                    "(default cargo: ${voyage.defaultCargo.content} - " +
                    "${voyage.defaultCargo.size}Kg) COMPLETE"
        }
    }
}
