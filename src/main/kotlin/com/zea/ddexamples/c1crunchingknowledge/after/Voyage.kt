package com.zea.ddexamples.c1crunchingknowledge.after

class Voyage(val itinerary: String, val capacity: Int, val defaultCargo: Cargo) {
    private val cargos = mutableListOf<Cargo>()

    fun addCargo(cargo: Cargo) {
        this.cargos.add(cargo)
    }
}
