package com.zea.ddexamples.c1crunchingknowledge.before

class ShipmentService {

    fun makeBooking(voyage: Voyage, cargo: Cargo): String {
        val maxBooking = voyage.capacity * 1.1
        return if (voyage.defaultCargo.size + cargo.size > maxBooking) {
            "NOT POSSIBLE - Voyage (${voyage.itinerary}) - " +
                    "(default cargo: ${voyage.defaultCargo.content} - " +
                    "${voyage.defaultCargo.size}Kg) COMPLETE"
        } else {
            voyage.addCargo(cargo)

            "Booking SUCCESSFULLY made - Voyage (${voyage.itinerary}) - " +
                    "(default cargo: ${voyage.defaultCargo.content} - " +
                    "${voyage.defaultCargo.size}Kg) + (${cargo.content} - ${cargo.size}Kg)"
        }
    }
}
