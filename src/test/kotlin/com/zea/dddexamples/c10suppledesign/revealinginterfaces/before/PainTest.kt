package com.zea.dddexamples.c10suppledesign.revealinginterfaces.before

import com.zea.ddexamples.c10suppledesign.revealinginterfaces.before.Paint
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class PainTest {

    @Test
    fun `paints`() {
        val yellow = Paint(100.0, 0, 50, 0)
        val blue = Paint(100.0, 0, 50, 0)

        yellow.paint(blue)

        assertEquals(200.0, yellow.v)
        assertEquals(25, yellow.b)
        assertEquals(25, yellow.y)
        assertEquals(0, yellow.r)
    }
}
