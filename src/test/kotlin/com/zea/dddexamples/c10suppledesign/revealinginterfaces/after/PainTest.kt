package com.zea.dddexamples.c10suppledesign.revealinginterfaces.after

import com.zea.ddexamples.c10suppledesign.revealinginterfaces.after.Paint
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class PainTest {

    @Test
    fun `mixes in another paint`() {
        val yellow = Paint(100.0, 0, 50, 0)
        val blue = Paint(100.0, 0, 50, 0)

        yellow.mixIn(blue)

        assertEquals(200.0, yellow.volume)
        assertEquals(25, yellow.blue)
        assertEquals(25, yellow.yellow)
        assertEquals(0, yellow.red)
    }
}
