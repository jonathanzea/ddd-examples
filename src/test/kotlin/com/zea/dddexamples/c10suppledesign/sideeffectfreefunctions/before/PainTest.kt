package com.zea.dddexamples.c10suppledesign.sideeffectfreefunctions.before

import com.zea.ddexamples.c10suppledesign.sideeffectfreefunctions.before.Paint
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class PainTest {

    @Test
    fun `paints`() {
        val yellow = Paint(100.0, 0, 50, 0)
        val blue = Paint(100.0, 0, 50, 0)

        yellow.mixIn(blue)

        assertEquals(200.0, yellow.volume)
        assertEquals(25, yellow.blue)
        assertEquals(25, yellow.yellow)
        assertEquals(0, yellow.red)
    }
}
