package com.zea.dddexamples.c10suppledesign.assertions.before

import com.zea.ddexamples.c10suppledesign.sideeffectfreefunctions.after.Paint
import com.zea.ddexamples.c10suppledesign.sideeffectfreefunctions.after.PigmentColor
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class PainTest {

    @Test
    fun `mixes in with another paint bucket`() {
        val paintBucket1 = Paint(100.0, PigmentColor(0, 50, 0))
        val paintBucket2 = Paint(100.0, PigmentColor(0, 50, 0))

        paintBucket1.mixIn(paintBucket2)

        assertEquals(200.0, paintBucket1.volume)
        assertEquals(PigmentColor(0, 25, 25), paintBucket1.pigmentColor)
    }
}
