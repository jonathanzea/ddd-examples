package com.zea.dddexamples.c10suppledesign.assertions.after

import com.zea.ddexamples.c10suppledesign.assertions.after.PigmentColor
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class PainTest {

    @Test
    fun `mixed paint return combined values`() {
        val yellow = PigmentColor(0, 50, 0)
        val blue = PigmentColor(0, 50, 0)

        val stockYellowPaint = StockPaint(25.5, yellow)
        val stockBluePaint = StockPaint(25.5, blue)

        val mixedPaint = MixedPaint(
            mutableListOf(stockYellowPaint, stockBluePaint)
        )

        assertEquals(51.0, mixedPaint.volume)
        assertEquals(PigmentColor(0, 25, 25), mixedPaint.pigmentColor)
    }
}
