package com.zea.dddexamples.c10suppledesign.assertions.after

import com.zea.ddexamples.c10suppledesign.sideeffectfreefunctions.after.PigmentColor
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class PigmentColorTest {

    @Test
    fun `mixes in with another color and produces combination`() {
        val yellow = PigmentColor(0, 50, 0)
        val blue = PigmentColor(0, 50, 0)
        val green = PigmentColor(0, 25, 25)

        assertEquals(green, yellow.mixedWith(blue))
    }
}
