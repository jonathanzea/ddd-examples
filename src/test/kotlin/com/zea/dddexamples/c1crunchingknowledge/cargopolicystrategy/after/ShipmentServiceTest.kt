package com.zea.dddexamples.c1crunchingknowledge.cargopolicystrategy.after

import com.zea.ddexamples.c1crunchingknowledge.after.Cargo
import com.zea.ddexamples.c1crunchingknowledge.after.OverBookingPolicy
import com.zea.ddexamples.c1crunchingknowledge.after.ShipmentService
import com.zea.ddexamples.c1crunchingknowledge.after.Voyage
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ShipmentServiceTest {

    @Test
    fun `allows add extra cargo of 10% overbooking`() {
        val voyage = Voyage(
            "USA-VZL",
            100,
            Cargo("Rice", 100)
        )
        val cargo = Cargo("Potato", 10)
        val booking = ShipmentService(OverBookingPolicy())
            .makeBooking(voyage, cargo)

        assertEquals(
            "Booking SUCCESSFULLY made - Voyage (USA-VZL) - (default cargo: Rice - 100Kg) + (Potato - 10Kg)",
            booking
        )
    }

    @Test
    fun `does not allow add extra cargo on full overbooking`() {
        val voyage = Voyage(
            "USA-VZL",
            100,
            Cargo("Rice", 110)
        )
        val cargo = Cargo("Potato", 10)
        val booking = ShipmentService(OverBookingPolicy())
            .makeBooking(voyage, cargo)

        assertEquals("NOT POSSIBLE - Voyage (USA-VZL) - (default cargo: Rice - 110Kg) COMPLETE", booking)
    }
}
