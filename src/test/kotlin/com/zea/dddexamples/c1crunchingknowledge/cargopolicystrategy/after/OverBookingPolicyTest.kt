package com.zea.dddexamples.c1crunchingknowledge.cargopolicystrategy.after

import com.zea.ddexamples.c1crunchingknowledge.after.Cargo
import com.zea.ddexamples.c1crunchingknowledge.after.OverBookingPolicy
import com.zea.ddexamples.c1crunchingknowledge.after.Voyage
import org.junit.jupiter.api.Test

class OverBookingPolicyTest {

    @Test
    fun `pass overbooking policy`() {
        val voyage = Voyage(
            "USA-VZL",
            100,
            Cargo("Rice", 100)
        )
        val cargo = Cargo("Potato", 10)
        val policy = OverBookingPolicy()

        assert(policy.isAllowed(voyage, cargo))
    }

    @Test
    fun `does not pass overbooking policy`() {
        val voyage = Voyage(
            "USA-VZL",
            100,
            Cargo("Rice", 110)
        )
        val cargo = Cargo("Potato", 10)
        val policy = OverBookingPolicy()

        assert(!policy.isAllowed(voyage, cargo))
    }
}
