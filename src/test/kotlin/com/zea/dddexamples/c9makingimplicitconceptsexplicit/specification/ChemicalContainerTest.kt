package com.zea.dddexamples.c9makingimplicitconceptsexplicit.specification

import com.zea.ddexamples.c9makingimplicitconceptsexplicit.specification.Container
import com.zea.ddexamples.c9makingimplicitconceptsexplicit.specification.ContainerFeature
import com.zea.ddexamples.c9makingimplicitconceptsexplicit.specification.Drum
import com.zea.ddexamples.c9makingimplicitconceptsexplicit.specification.Nitroglycerin
import org.junit.jupiter.api.Test

class ChemicalContainerTest {

    @Test
    fun `explosive chemical is satisfied by armored container`() {
        val nitroglycerinDrum = Drum(Nitroglycerin())
        val container = Container().withFeature(ContainerFeature.ARMORED)

        assert(nitroglycerinDrum.chemicalSpecification().isSatisfiedBy(container))
    }

    @Test
    fun `explosive chemical is not satisfied on not armored container`() {
        val nitroglycerinDrum = Drum(Nitroglycerin())
        val container = Container()

        assert(!nitroglycerinDrum.chemicalSpecification().isSatisfiedBy(container))
    }
}
