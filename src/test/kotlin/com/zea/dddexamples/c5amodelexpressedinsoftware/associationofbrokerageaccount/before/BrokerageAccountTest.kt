package com.zea.dddexamples.c5amodelexpressedinsoftware.associationofbrokerageaccount.before

import com.zea.ddexamples.c5amodelexpressedinsoftware.before.BrokerageAccount
import com.zea.ddexamples.c5amodelexpressedinsoftware.before.Customer
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class BrokerageAccountTest {

    @Test
    fun `return investments for account`() {
        val customer = Customer("538860LLP")
        val account = BrokerageAccount("1123", customer)

        account.order("S&P500", 32)
        account.order("MSCI", 12)

        assertEquals(
            "Customer: 538860LLP / Account: 1123 - S&P500 x32, " +
                    "Customer: 538860LLP / Account: 1123 - MSCI x12",
            account.getInvestmentsSummary()
        )
    }
}
