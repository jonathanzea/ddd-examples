package com.zea.dddexamples.c5amodelexpressedinsoftware.associationofbrokerageaccount.after

import com.zea.ddexamples.c5amodelexpressedinsoftware.after.BrokerageAccount
import com.zea.ddexamples.c5amodelexpressedinsoftware.after.Customer
import com.zea.ddexamples.c5amodelexpressedinsoftware.after.Investment
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class BrokerageAccountTest {

    @Test
    fun `return an investment searching by stock symbol`() {
        val account = BrokerageAccount(
            "1123",
            Customer("201122202GM")
        )
        account.order(Investment("S&P500", 32))

        val investment = account.getInvestment("S&P500")
        assertEquals("Customer 201122202GM / Account: 1123 - S&P500: 32", investment)
    }
}
