import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
// ./gradlew wrapper --gradle-version=6.7.1

plugins {
    id("org.jetbrains.kotlin.jvm") version "1.3.71"
    id("org.jlleitschuh.gradle.ktlint") version "9.1.1"
    id("jacoco") // version tied to gradle version
}

group = "com.zea"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib:1.4.21")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.7.0")
}

tasks {
    withType<Test> {
        useJUnitPlatform()
        testLogging {
            showExceptions = true
            exceptionFormat = org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
            showStandardStreams = true
            events("passed", "skipped", "failed")
        }

        withType<KotlinCompile> {
            kotlinOptions {
                jvmTarget = "12"
            }
        }

        withType<JacocoReport> {
            reports {
                xml.isEnabled = false
                csv.isEnabled = false
                html.destination = file("$buildDir/site")
            }
            // dependsOn(test) // Specify when you want to insert the jacoco report between other tasks
        }
        finalizedBy("jacocoTestReport") // report is always generated AFTER TESTS TASK RUN
    }
}
