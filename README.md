# Domain-Driven Design Examples :blue_book:	

In this small project we use `Gradle` and `Kotlin` to write the examples given in the _Domain-Driven Design_ book by 
_Eric Evans_. The package structure is divided by chapter number and name along with subpackages containing each example
name. Inside you have a `before` package with the original example code and `after` package with the refactored version
following the good design principles.

[[_TOC_]]
# Book Summary

## A Model Expressed in the Software
The building blocks for the Domain Model. They are your tools to express the domain:

### Layered Architecture
* With the growing complexity of Software the industry, in order to stablish some *order* came up with a **Layered Architecture** approach:
  * UI
  * Application
  * Domain
  * Infrastructure

### Frameworks
* Be careful when implementing Frameworks in your system. They easily can stablish a **Totalitarian Regime** in your project. 
* They will obfuscate any effort of having a **domain model**
* They will leave the project code with pure infrrastructure and technical gibberish.

### Associations
* Constraint them and *avoid* them as much as possible. Remove those which are unnecesary.
* Imposte **traversal** associations (simple - unidirectional).
* Add qualifiers between associated objects to avoid duplicity (one-to-many-relationships).

## Patterns of Model Elements
### Entities
* Objects with an **abstract continuity**.
* When dealinf with Entities keep the **relationships** between them as **simple** as possible.
* They have an ID. An identity provided by some *Generator*. Therefore, this identity needs to be **guaranteed to be unique**.
* Their instances should not be shared.
* A Java object has, for example, an intrinsec identity. The memory location. Completely useless, though, in a domain model context.
* Remove as much behaviour as possible from Entities.

### Value Objects
* They have **no abstract continuity.**. They have no identity.
* Represent transient state.
* Can reference Entities. (eg. TravelRoute Object. Has two properties of type City, origin and destination. City is an Entity. TravelRoute is a VO).
* Their instsance can be shared in case of performance requirements.

### Services
* **Represent the Verbs**, the Do's, the Actions **of the domain model**. This verbs should belong to the *Ubiqutous Language*.
* They are stateless.
* They are not ment to rip-of the behavior of all the objects. They must be highly cohesive and lowly coupled.
* Their instance can be shared.
* Some Services are intended to solve some technical problems not necessarily related to the domain model. Therefore, we can clasify the Services in layers:
   * Domain Services
   * Application Services
   * Infrastructure Services 
* When creating the Services **avoid small granularity**. Otherwise, you'll endup with a bunch of Infrastructure Serivces. As a result, not revealing any Domain insight.

### Modules
* Commonly represented by the **Packages** of your project. Should be the ones responsible of showing a **high-level and not-overwhelming** overview of your application.
* If your application is a book. Then the Modules are its chapters. They will increase the cohesion and domain concepts your application handles. Therefore, the package names should contain big part of the Ubiqutous Language.
* When creating objects keep an eye on the imports. You will instsantly detect any strange conceptual relationships.
* They need to be also refactored as the application grows and iterate.
* Totalitarian and intrusive Frameworks can affect the package configurations. Try to balance this. Never give up on creating **meaningul** pieces for the **model**. 

## Modeling Paradigms
* Many different paradigms have been tried (eg. Rule Engines). Only a few have succeeded like **OOP**.
* Anything you choose must be focused on something that express your domain in the model.
* Make sure it has a large community. Guarantee its maturity. Be careful when choosing cutting-edge technology systems. It could easily affect productivity and bring unexpecteded design problems in the future.
* OOP is the prefered option when modeling complex systems because of its flexibility.
* Your application can have **more than one paradigm**. However, it's not easy to mantain and rapidly will gain complexity. Try to leverage on Ubiqutous Language to keep a consistent model.

## The Life Cycle of a Domain Object
When modeling real complex enterprise system you will come accross with very complicated objects. Objects that have a long lifecycle which will easily derail you from Model Driven Design. To face this challenge and mantain these complex objects integrity these **Patterns to Manipulate Objects** can be used:

### Aggregates
* Simplify complex object associations.
* Helps to keep these associations as **a mean to reveal the domain**.
* It's a cluster of object. These objects are surrounded (metaforically) by a **Boudary** and the objects can this boundary can be only accessed through a **Root** object.
* The Root entity object has a *global identity* and it's responsible to keep the **invariants** (object valudations and rules) inside the Cluster.
* Entity objects inside the boundary have only local identity. They unique only fron the Aggregate point of view.
* Nothing outside the Aggregate can access anything inside. They can only talk to the Root entities.
* Only Agregate roots can be queried.

### Factories
* Simplify the **creation** of complex objects in a **consistent** way.
* The Factory can be a standalone object or can be simply a method in the object.
* When the Factory creates an Aggregate must guarantee that the object is created with all the invariants satisfied and properly initialized. If the Facttory product is a normal Entity this initialization means the ID of the Entity set and the object ready to be populated. If the product is a Value Object, which are inmutable, then the object must be atomically and returned as an inmutable consistent object.
* Factories are in the Domain layer.
* If the objects are simple enough just use **normal constructors**.
* Since the Factories will be always tightly coupled to its parameters, try to make this coupling something meaninful. Avoid passing primitives in the parameters but revealing objects.
* A Factory can be a Reconstituing Factory. These are the ones that instantiate existing object in different contexts. For example, the Repository is a Reconsituing Factory because its product is an object which data comes from the Data Base. The same with REST APIS that deserialize data into objects.

### Repositories
* A simple conceptual framework to encapsulate object querying solutions. This objects come from a persistent storage.
* With the Repository you can not only query, but also modify or delete objects without revealing the complex persistence infrastructure behind (Database).
* The object the Repository encapsulate must be a virtual collection of an Aggregate. Once you query it you can access it respecting it's boundary and associations.
* The Repository can easily contain *hardcoded* queries that matches certain Criterias. Or you could have some more sophisticated approaches like Specification Based Queries.
* If you are not careful when desigining the Repository you could be querying objects that brings a huge amount of data from the persistence layer and running into performance problems.
* Relational database systems allow you to perform sophisticated association mechanism between entities. However, you will have to sacrifice these capabilities to create a Model, both in the program and in the database, that are as equal as possible. Otherwise you will have **different Models or representations of one same Domain**.
* Don't allow any other system that is not your program to modify objects in the storage. It will create inconsistencies.
* Kepp the *Transaction Management* code outside the Repositories. This can easilty clutter the class. Repositories are a good place to reveal the Domain and its principal entities or Agregate.
* A Respository is a Factory. A Reconstituing Factory.

## Refactoring Towards a Deeper Model
The **basic building blocks** were mentioned above. Now, we want to proceed to **capture a deep understanding of the Domain**. Successfull Models worth the effort they demand. They can be only achieved through an Iterative Process, **refactoring** and close domain expertise. However, they might require some level of sophisitcation in coding skills.

### Levels of Refactoring
* Refactor is to redesign but without changing the functionality.
* Micro-refactorings improve the readability of the code. However, more complex refactors motivated to increase the Insight of the Domain will reveal the explicit intention of the code.

### Deep Models
* In the intial stage of a Domain Analysis you identify *Vebrs* and *Nouns* which you then put into objects.
* After deepening in the Domain you will realise this initial understanding is nahive.
* Therefore, the Model will constantly evolve. Your goal needs to be achieve lucid expression, simplicity, versatility and explanatory power.
* The Model Design needs to tolerate this constant changes, it needs to flexible, resilient and strong at the same time. Here, we can se the *Glove Metaphor*. Your system is like a glove. Flexible in the articulation areas and steady and robust in the *static* parts.
* In a Domain Driven Design a Deep Model makes possible to have an expressive design while this expressiveness feeds insight into the model. Is a cycle where both parts constantly iterate each other.
* When the code is hard to refactor, the whole moleding process comes to a halt.

### Making implicit Concepts Explicit
When understaing the Domain Logic behind the code is not-so-obvious this 3 categories of Model Concept could help you to improve the design:
1. **Explicit Constants**: Badly design **methods** tend to have a **low cohesion**. Through all the scattered algorith you might identify that an Invariant is been checked. **Extract** this **Invariant** Logic to it's own method. Invariants are a **great way to revel Domain Logic**. This will give the constraint its own place to grow, evolve and articulate. The constraint or invariant could grow enough to own its own class. If you see the object enclosing the invariant requires data that is only used in that method, or if this rule appears in multiple objects, then is time to give the constraint its own class.

2. **Processes as Domain Objects**: When you have a *Service* in which the main algoritm has many branches you might want to create a **Strategy**. This way, you can split the big process in smaller parts that will reveal a richer Domain Logic and will help the Model to cover and show important characteristics of the system.

3. **Specification**: Specifications are a more sophisitaced way to create Validations or Invariants. You can convert them into Predicates that determines what criteria objects have to satisfy. Specifications also reveal explicit rules and boundaries in the Domain Model.

### Supple Design
A Supple Design adds flexibility to your model in the parts that it's needed. We can use the "Glove Metaphor" again. Your Model will have areas that will change more often than others. This parts of our model need to be designed thinking on those upcoming constant changes. For that you need a Supple Flexible Design. Some of the Patterns you can use to lead your design towards it:

#### Intention-Revealing Interfaces
* Name Classes and operations to describe their effect and purpose. Avoid references to the internal nitty-gritty technical details of it.
* Start writting tests for the expected behaviour. This way you force your thinking into a Client Developer Mode.

#### Side-effec-free Functions
* Operations on your system can be broadly divided into **Queries Functions** and **Command Functions**. Query functions return results without producing any side-effects. Commands produce side-effects on the systems.
* Keep Command Functions and Quieries Functions strictly seggregated. Avoid to return from Commands. And, avoid causing side effect from Query Functions.
* If an object has a Query Function that contains complicated calculation loginc then move it into a Value Object.

#### Standalone Classes
Decrease coupling and increase cohesion by putting dependent classes in the same package. Give meaningful names to each of them. An Intenger field in a class is a dependency. However, its dependency is completely necessary and understandable. So, if you have objects that are meaningless without its depdent class or vice-versa, then put them all in the same package.

#### Closure of Operations
Dealing with only primitive values impoverish the desigh. Define operatios whose return type is the same as the type of the arguments. This provides a more meaning-revealing desing.

### Mantaining Model integrity
To keep a Model internally consistent avoid overlapping definitions, contradictory concepts by taking decisions on **what should be unified** and what should be kept apart. The following technigques will help you **recognize**, communicate and chose the **limits of a model**:

#### Bounded context
* In large projects, multiple Models are always in place. However, the real problem starts in the way Teams are organized and the way people interact.
* A Bounded Context define explicitely the scope of a particular Model.
* Working with a *Bounded Context gives clarity and freedom** and avoid "gray zones" in the Model.
* Some of the symptoms of unrecognized Bounded Context are: 
  * Duplication of Concepts: This is visible when some Business Logic is changed in one place and exact same code or logic must be also updated in many other places
  * False Cognates: People using the same term but they are refering to something else in the Domain.

#### Continous Integration:
* To mantain model integrity within the Bounded Context avoid to accumulate code changes without deploying. Otherwise, the probabilites of making Model mistakes increase. Decrease the lifetime of non-integrated changes
* Usse CI pipeline techniques to constantly deploy.
* Hace a reproducible build-deploy in your pipelines.
* Have automated test suites.
* Make a constant exercise of Ubiqutous Language.

#### Context Maps
A Context Map the point of contact between Bounded Contexts. **Code Reuse** between Bounded Contexts is a hazard to avoid. To communicate two or more Bounded Contexts you must create a **Translation Layer**. There are some **Patterns** you could use to **develop** a proper **Context Map**:

##### Shared Kernel
In some big Domain Cores where you have many Bounded Contexts the overlapping of these is easy to happen evertime. Explecitely designate a Subset of the Domain that will be agreed to be shared between Bounded Contexts. This Shared Module requres a special status and shall not be changed without consultation to the teams involved.

##### Customer-supplier Development Teams
Sometimes the Bounded Contexts are shared by temas that implement **different technologies**. For example, the "Business Inteligence" team analises data that flows through your core applications with different persistence, monitoring and data management and transformation tools and langugages. And, also that data is produced by the core systems. Therefore, the core services will be the Supplier and the BI team the Customer. 

The Shared Kernel cannot be used in this case becuase that pattern is only for teams that use the same technologies.

Then, stablish a proper relationship between the two teams. Automate Acceptance TEsts to check that nothing is breaking or blocking the consumer team.

##### Conformist
This pattern is seen hen the Upstream Team (Supplier) has "no motivation" to privovide the Downstream Team (Customer) with no "translation layer". Therefore, delete all the complexity. of translation layers and become a "Slave" shared kernel.

##### Anti-corruption Layer
* It happens that new system is being built and it needs to mantain an Interface with another Legacy System. In such case, build an Anticorruption Layer between both of them.
* The antique Model will be always different to the new one, the created layer must respect the new system model of the Domain to avoid Leaking Concepts between them.
* Avoid operating in Low-level-primitive Interfaces. Capture and encapsulate high level Domain Meaning with objects.
* Avoid representing the Legacy System in only 1 (one) Interface. Create a more articulated layer that represent differnet views of the Legacy.
* Use Facades, Adapters and Translators patterns to implement the Anticorruption Layer.

##### Separate Ways
Sometimes the Integration Layer provides no significant benefit. In that case, simply remove the integrations or create e new Bounded Context for the overlapped area.

##### Open Host Service and Published Language
When your system is consumed by many other Bounded Contexts creating a translator layer for each of them can be too much. For that, create a Set of Services and some sort of langusage (Ubiqutous Language) for the context. This way, all the clients communicate the same way. It's basically declaring a new protocol.

### Distillation
The following are approaches to Distille the Core Domain:
* **Core Domain**: Boil the model down and find what the Core Domain is. Identify the principal and protagonist areas of your Domain Model that represents the Domain from a high view level.
* **Generic Subdomains**: Don't Spend tome and energy developing specialitiess that are not your Domain primary focus (Things like Date management libraries, Time zones handlers, Sheets managers). Consider "out-of-the-shelf solution for such a low level technicalities if they represent a challenge. Focus in the Core Domain.
* **Cohesive Mechanisms**: Serves to encpsulate more complex operations and algorithms and this way, leaving more visible only the Core Domain operations.

# Gitlab CI

There's a simple CI file with a build job and a test job that also prints the 
code coverage for the gitlab badge purpose.

Enjoy.
_Jonathan Zea - December 2020 - Berlin_ :ocean:
